<?php

namespace TBaronnat\SecurityBundle\Extension;

use TBaronnat\SecurityBundle\Manager\SecurityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigSecurityExtension extends AbstractExtension
{
    public function __construct(public readonly SecurityManagerInterface $securityManager)
    {}

    public function getFunctions(): array
    {
        return [
            new TwigFunction('is_action_granted', [$this->securityManager, 'isActionGranted']),
            new TwigFunction('is_action_granted_for_route', [$this->securityManager, 'isActionGrantedForRoute']),
        ];
    }
}
