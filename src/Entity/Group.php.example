<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use TBaronnat\SecurityBundle\Entity\SecurityGroupInterface;
use TBaronnat\SecurityBundle\Entity\SecurityUserInterface;

class Group implements SecurityGroupInterface
{
    private ?string $name = null;

    private ?array $roles = [];

    protected ?Collection $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getUsers(): ?Collection
    {
        return $this->users;
    }

    public function addUser(SecurityUserInterface $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addGroup($this);
        }

        return $this;
    }

    public function removeUser(SecurityUserInterface $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeGroup($this);
        }

        return $this;
    }

    public function hasRole(string $role): bool
    {
        if (empty($this->roles)) {
            return false;
        }
        return false !== $key = array_search($role, $this->roles, true);
    }

    public function addRole(?string $role): self
    {
        if (!$this->hasRole($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(?string $role): self
    {
        if (false !== $key = array_search($role, $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    public function getRoles(): ?array
    {
        if ($this->roles !== null) {
            asort($this->roles);
        }
        return $this->roles;
    }

    public function setRoles(?array $roles = []): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
