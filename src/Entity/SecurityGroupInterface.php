<?php

namespace TBaronnat\SecurityBundle\Entity;

interface SecurityGroupInterface
{
    public function hasRole(string $role): bool;
}
