<?php

namespace TBaronnat\SecurityBundle\Entity;

interface SecurityUserInterface
{
    public function getGroups();
}