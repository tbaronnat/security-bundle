<?php

namespace TBaronnat\SecurityBundle\Listener;

use TBaronnat\SecurityBundle\Manager\SecurityManagerInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class SecurityListener implements SecurityListenerInterface
{
    public function __construct(protected readonly SecurityManagerInterface $securityManager)
    {}

    public function supports(?string $route): bool
    {
        if (strpos($route, 'login') || strpos($route, 'logout') || $route === null) {
            return false;
        }

        return str_contains($route, 'admin_');
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $route = $event->getRequest()->attributes->get('_route');
        if ($this->supports($route)) {
            if (!$this->securityManager->isGranted($route)) {
                throw new AccessDeniedException();
            }
        }
    }
}
