<?php

namespace TBaronnat\SecurityBundle\Listener;

use Symfony\Component\HttpKernel\Event\RequestEvent;

interface SecurityListenerInterface
{
    public function supports(?string $route): bool;
    public function onKernelRequest(RequestEvent $event): void;
}