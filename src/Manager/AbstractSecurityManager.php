<?php

namespace TBaronnat\SecurityBundle\Manager;

use TBaronnat\SecurityBundle\Entity\SecurityGroupInterface;
use TBaronnat\SecurityBundle\Entity\SecurityUserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

abstract class AbstractSecurityManager implements SecurityManagerInterface
{
    public function __construct(
        protected readonly RouterInterface $router,
        protected readonly TokenStorageInterface $tokenStorage
    ) {}

    public function getRoutesWithRoles(): array
    {
        $availablePath = $this->getAvailablePath();
        $excludedPath = $this->getExcludedPath();
        $routes = [];
        foreach ($this->router->getRouteCollection()->all() as $routeName => $route) {
            $roleName = $this->getRoleName($routeName);
            if (($this->isRoleMatching($routeName, $availablePath)
                    && !$this->isRoleMatching($routeName, $excludedPath))
                || (empty($availablePath) && empty($excludedPath))
            ) {
                $mainRoleName = $this->getMainRoleName($routeName);
                $routes[$mainRoleName] = $mainRoleName;
                $routes[$roleName] = $roleName;
            }
        }

        asort($routes);

        return $routes;
    }

    public function isGranted(string $routeName): bool
    {
        $token = $this->tokenStorage->getToken();
        if ($token === null) {
            return false;
        }

        $user = $token->getUser();
        if (!$user instanceof SecurityUserInterface) {
            return false;
        }

        $routesWithRoles = $this->getRoutesWithRoles();

        /** @var SecurityGroupInterface $group */
        foreach ($user->getGroups() as $group) {
            if (!in_array($this->getRoleName($routeName), $routesWithRoles)) {
                continue;
            }
            if ($group->hasRole($this->getRoleName($routeName))
                ||
                $group->hasRole($this->getMainRoleName($routeName))
            ) {
                return true;
            }
        }

        return false;
    }

    public function isActionGrantedForRoute(string $routeName, string $action): bool
    {
        return $this->isGranted(
            sprintf('%s_%s', $this->getBaseRouteName($routeName), $action)
        );
    }

    public function isActionGranted(Request $request, string $action): bool
    {
        return $this->isActionGrantedForRoute(
            $request->attributes->get('_route'),
            $action
        );
    }

    protected function getRoleName(string $routeName): string
    {
        return SecurityManagerInterface::ROLE_NAME_PREFIX.strtoupper($routeName);
    }

    protected function getRoleNameFromRouteAndAction(string $routeName, string $action): string
    {
        return strtoupper(
            sprintf('%s_%s',
                $this->getRoleName($this->getBaseRouteName($routeName)),
                $action
            ),
        );
    }

    protected function getMainRoleName(string $routeName): string
    {
        return $this->getRoleNameFromRouteAndAction($routeName, SecurityManagerInterface::ROLE_ALL_SUFFIX);
    }

    protected function getBaseRouteName(string $routeName): string
    {
        foreach ($this->getAvailablePath() as $action) {
            $routes = explode($action, $routeName);
            if (2 == count($routes)) {
                return $routes[0];
            }
        }

        return $routeName;
    }

    protected function isRoleMatching(string $routeName, ?array $containString = []): bool
    {
        foreach ($containString as $string) {
            if (false !== stripos($routeName, $string)) {
                return true;
            }
        }

        return false;
    }

    abstract protected function getAvailablePath(): array;

    abstract protected function getExcludedPath(): array;
}
