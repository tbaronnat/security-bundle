<?php

namespace TBaronnat\SecurityBundle\Manager;

use Symfony\Component\HttpFoundation\Request;

interface SecurityManagerInterface
{
    public const DEFAULT_ACTIONS = ['_index', '_create', '_update', '_delete', '_show'];
    public const ROLE_ALL_SUFFIX = 'ALL';
    public const ROLE_NAME_PREFIX = 'ROLE_';

    public const ACTION_BUTTON_UPDATE = 'update';
    public const ACTION_BUTTON_CREATE = 'create';
    public const ACTION_BUTTON_DELETE = 'delete';
    public const ACTION_BUTTON_SHOW = 'show';
    public const ACTION_BUTTON_INDEX = 'index';

    public function getRoutesWithRoles(): array;

    public function isGranted(string $routeName): bool;

    public function isActionGrantedForRoute(string $routeName, string $action): bool;

    public function isActionGranted(Request $request, string $action): bool;
}
