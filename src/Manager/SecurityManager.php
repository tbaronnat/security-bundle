<?php

namespace TBaronnat\SecurityBundle\Manager;

class SecurityManager extends AbstractSecurityManager implements SecurityManagerInterface
{
    protected function getAvailablePath(): array
    {
        return SecurityManagerInterface::DEFAULT_ACTIONS;
    }

    protected function getExcludedPath(): array
    {
        return ['partial', 'profiler'];
    }
}
