<?php

namespace TBaronnat\SecurityBundle;

use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TBaronnat\SecurityBundle\DependencyInjection\SecurityExtension;

class TBaronnatSecurityBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        if ($this->extension == null) {
            $this->extension = new SecurityExtension();
        }

        return $this->extension;
    }
}