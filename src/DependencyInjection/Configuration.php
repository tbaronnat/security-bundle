<?php

namespace TBaronnat\SecurityBundle\DependencyInjection;


use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration implements ConfigurationInterface
{
    function getConfigTreeBuilder(): TreeBuilder
    {
        return new TreeBuilder('security_bundle');
    }
}