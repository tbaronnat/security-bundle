Example implementation : 

Override default SecurityManager to add or exclude routes paths :

services.yaml : 

      tbaronnat.manager.security:
        class: App\Manager\YourOwnSecurityManagerThatExtendsAbstractSecurityManager
        arguments:
          - '@router'
        public: true


Override default SecurityListener to overide method "supports" and apply security for specific routes :

services.yaml :
     
      tbaronnat.listener.admin.security:
        class: App\Listener\YourOwnSecurityListenerThatExtendsSecurityListener
        arguments: 
            - '@tbaronnat.manager.security'
        public: true
        tags:
          - { name: kernel.event_listener, event: kernel.request, method: onKernelRequest }


Entities : 

User class must implements :

        TBaronnat\SecurityBundle\Entity\SecurityUserInterface
        
        
Groups class must implements :

        TBaronnat\SecurityBundle\Entity\SecurityGroupInterface


That IT